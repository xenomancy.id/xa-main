<?php

/**
 * Use Markdown Markup in PmWiki
 *
 * @author Sebastian Siedentopf <schlaefer@macnews.de>
 * @version 2018-01-05
 * @link http://www.pmwiki.org/wiki/Cookbook/MarkdownMarkupExtension
 * @copyright by the respective authors 2006
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @package markdownpmw
 *
 * Version 0.2 and later by Said Achmiz
 */

$RecipeInfo['Markdown Markup Extension']['Version'] = '2018-01-05';

SDV($MarkdownMarkupParserOptions, array());
SDV($MarkdownMarkupMarkdownExtraEnabled, false);
SDV($MarkdownMarkupUseClassicParser, false);

Markup("markdown", '<include', "/\(:markdown:\)(.*?)[\n]?\(:markdownend:\)/s",
	"MarkupPmWikiConversion");

function MarkupPmWikiConversion($m) {
	$text = $m[1];
	$astr = array (
			"<:vspace>" => "\n\n",
			"(:nl:)" => "\n",
			"&gt;" => ">",
			"&lt;" => "<",
		);
	$pstr = array(
// 			"/<p>/" => "<p class='vspace'>",
			"/&amp;(.*?);/" => "&\\1;",
		);

	$text = str_replace(array_keys($astr), $astr, $text);

	global $MarkdownMarkupUseClassicParser;
	if ($MarkdownMarkupUseClassicParser) {
		define(MARKDOWN_PARSER_CLASS, ($MarkdownMarkupMarkdownExtraEnabled ? 'MarkdownExtra_Parser' : 'Markdown_Parser'));
		include_once("markdown.php");

		$text = Markdown($text);
	} else {
		if ($MarkdownMarkupMarkdownExtraEnabled) {
			include_once("Michelf/MarkdownExtra.inc.php");
		} else {
			include_once("Michelf/Markdown.inc.php");
		}

		global $MarkdownMarkupParserOptions;
		if (empty($MarkdownMarkupParserOptions)) {
			$text = $MarkdownMarkupMarkdownExtraEnabled ?
					Michelf\MarkdownExtra::defaultTransform($text) :
					Michelf\Markdown::defaultTransform($text);
		} else {
			$parser =  $MarkdownMarkupMarkdownExtraEnabled ? new Michelf\MarkdownExtra : new Michelf\Markdown;
			foreach ($MarkdownMarkupParserOptions as $md_opt => $md_opt_value) {
				$parser->{$md_opt} = $md_opt_value;
			}
			$text = $parser->transform($text);
		}
		$text = preg_replace(array_keys($pstr), $pstr, $text);
	}

	return Keep($text);
}

?>
