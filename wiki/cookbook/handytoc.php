<?php if (!defined('PmWiki')) exit();
/*
 * Copyright 2006 Kathryn Andersen
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the Gnu Public Licence or the Artistic Licence.
 */ 

/** \file handytoc.php
 * \brief JavaScript Table of Contents
 *
 * See also: http://www.pmwiki.org/wiki/Cookbook/HandyTableOfContents
 *
 * Markup changed for PHP 7.2 compatibility by Hans Bracker 
 * $HandyTocAnchorAfterElement & $HandyTocDefaultTitle features added by Said Achmiz (& bug fixes)
 * $HandyTocSmartAnchors added by Said Achmiz
 * $HandyTocExcludeWithin added by Said Achmiz
 */
$RecipeInfo['HandyTableOfContents']['Version'] = '2020-05-01';

SDV($HandyTocPubDirUrl, '$FarmPubDirUrl/handytoc');
SDV($HandyTocStartAt, 1);
SDV($HandyTocEndAt, 6);
SDV($HandyTocIgnoreSoleH1, 'true');
SDV($HandyTocAnchorAfterElement, true);
SDV($HandyTocSmartAnchors, false);
SDV($HandyTocDefaultTitle, '');
SDV($HandyTocExcludeWithin, '');

# Markup("handytoc", "directives", "/\\(:htoc\s*(.*):\\)/", "HandyTocProcessMarkup");
Markup("handytoc", "directives", 
  "/\\(:htoc\s*(.*?):\\)/",  //added ? after *
  "HandyTocProcessMarkup");
  
function HandyTocProcessMarkup($m) {
	global $HTMLHeaderFmt;
	$HTMLHeaderFmt['handytoc'] = 
	   "<link rel='stylesheet' href='\$HandyTocPubDirUrl/handytoc.css' type='text/css' />\n";
	$HTMLHeaderFmt['handytoc'] .= " 
	  <script language='javascript' type='text/javascript' src='\$HandyTocPubDirUrl/handytoc.js'></script>";

	global $HandyTocEndAt, $HandyTocStartAt, $HandyTocIgnoreSoleH1, $HandyTocAnchorAfterElement, $HandyTocSmartAnchors, $HandyTocDefaultTitle, $HandyTocExcludeWithin, $HTMLFooterFmt;
	extract($GLOBALS['MarkupToHTML']);
	$args = ParseArgs($m[1]);
	$title = ($args[''] ? implode(' ', $args['']) : $HandyTocDefaultTitle);
	$start = ($args['start'] ? $args['start'] : $HandyTocStartAt);
	$end = ($args['end'] ? $args['end'] : $HandyTocEndAt);
	$ignoreh1 = ($args['ignoreh1'] ? $args['ignoreh1'] : $HandyTocIgnoreSoleH1);
	$anchorafterelement = ($args['anchorafterelement'] ? $args['anchorafterelement'] : ($HandyTocAnchorAfterElement ? 'true' : 'false'));
	$smartanchors = ($args['smartanchors'] ? $args['smartanchors'] : ($HandyTocSmartAnchors ? 'true' : 'false'));
	$excludewithin = ($args['excludewithin'] ? $args['excludewithin'] : $HandyTocExcludeWithin);
	$class = ($args['class'] ? ' class="' . $args['class'] . '" ' : '');
	$script_set_args = "<script language='javascript' type='text/javascript'>TOC.set_args({start:$start, end:$end, ignoreh1:$ignoreh1, anchorafterelement:$anchorafterelement, smartanchors:$smartanchors, excludewithin:'$excludewithin'});</script>";
	if ($title) {
		return Keep("$script_set_args<div ${class}id='htoc'><h3>$title</h3></div>");
	} else {
		return Keep("$script_set_args<div ${class}id='htoc'></div>");
	}
}
