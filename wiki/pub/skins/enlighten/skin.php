<?php if (!defined('PmWiki')) exit();
/* PmWiki Enlighten skin
 *
 * Examples at: http://pmwiki.com/Cookbook/Enlighten and http://solidgone.org/Skins/
 * Copyright (c)2016 David Gilbert
 * This work is licensed under a Creative Commons Attribution-Share Alike 4.0 International License.
 * Please retain the links in the footer.
 * http://creativecommons.org/licenses/by-sa/4.0/
 * 
 * Updated for PHP 7.2 by Petko Yotov 20181203
 * Parts derived from stdmarkup.php
 */
global $FmtPV;
$FmtPV['$SkinName'] = '"Enlighten"';
$FmtPV['$SkinVersion'] = '"1.1.3"';

global $PageLogoUrl, $PageLogoUrlHeight, $PageLogoUrlWidth, $HTMLStylesFmt ,$SkinTheme;
if (!empty($PageLogoUrl)) {
	dg_SetLogoHeightWidth(15, 16);
	$HTMLStylesFmt['enlighten'] .=
		'#logo-box .sitetitle a{ height:' .$PageLogoUrlHeight .'; background: url(' .$PageLogoUrl .') left 16px no-repeat; } '.
		'#logo-box .sitetitle a, #logo-box .sitetag{ padding-left: ' .$PageLogoUrlWidth .'; } ';
}
global $SkinWidth,$SkinSidebarWidth,$SkinWidthUnit,$SkinHeaderBackground,$SkinDirUrl;
SDV($SkinWidth,850);
SDV($SkinSidebarWidth,212);  #good percentage width is 25
SDV($SkinWidthUnit,'px');  #only use 'px' or '%'
$HTMLStylesFmt['enlighten'] .=	'#wrap { width: '.$SkinWidth.$SkinWidthUnit.'; } '.
	'#sidebar { width: '.$SkinSidebarWidth.'px; margin-left: -'.($SkinSidebarWidth+10).'px; } '.
	'#content { margin-right: '.($SkinSidebarWidth+2).'px; } ';
$SkinColor = dg_SetSkinColor('brown', array('brown','green','orange','blue'));

SDV($SkinHeaderBackground, 'headerphoto.jpg');
$HTMLStylesFmt['enlighten'] .= '#header .headerphoto { background: #FFF url('.$SkinDirUrl.'/'.$SkinHeaderBackground.') no-repeat; } ';

# ----------------------------------------
# - Standard Skin Setup
# ----------------------------------------
$FmtPV['$WikiTitle'] = '$GLOBALS["WikiTitle"]';
$FmtPV['$WikiTag'] = '$GLOBALS["WikiTag"]';

# Move any (:noleft:) or SetTmplDisplay('PageLeftFmt', 0); directives to variables for access in jScript.
$FmtPV['$LeftColumn'] = "\$GLOBALS['TmplDisplay']['PageLeftFmt']";
$FmtPV['$RightColumn'] = "\$GLOBALS['TmplDisplay']['PageRightFmt']";
$FmtPV['$ActionBar'] = "\$GLOBALS['TmplDisplay']['PageActionFmt']";
$FmtPV['$TabsBar'] = "\$GLOBALS['TmplDisplay']['PageTabsFmt']";
$FmtPV['$SearchBar'] = "\$GLOBALS['TmplDisplay']['PageSearchFmt']";
$FmtPV['$TitleGroup'] = "\$GLOBALS['TmplDisplay']['PageTitleGroupFmt']";

Markup('notabs', 'directives',  '/\\(:notabs:\\)/i', "mu_enlighten_skin");
Markup('nosearch', 'directives',  '/\\(:nosearch:\\)/i', "mu_enlighten_skin");
Markup('notitlegroup', 'directives',  '/\\(:notitlegroup:\\)/i', "mu_enlighten_skin");
Markup('fieldset', 'inline', '/\\(:fieldset:\\)/i', "<fieldset>");
Markup('fieldsetend', 'inline', '/\\(:fieldsetend:\\)/i', "</fieldset>");


function mu_enlighten_skin($m) {
  extract($GLOBALS["MarkupToHTML"]);
  switch ($markupid) {
    case 'notabs': return SetTmplDisplay('PageTabsFmt',0);
    case 'nosearch': return SetTmplDisplay('PageSearchFmt',0);
    case 'notitlegroup': return SetTmplDisplay('PageTitleGroupFmt',0);
  }
}
# Override pmwiki styles otherwise they will override styles declared in css
global $HTMLStylesFmt;
$HTMLStylesFmt['pmwiki'] = '';

# Add a custom page storage location
global $WikiLibDirs;
$PageStorePath = dirname(__FILE__)."/wikilib.d/{\$FullName}";
$where = count($WikiLibDirs);
if ($where>1) $where--;
array_splice($WikiLibDirs, $where, 0, array(new PageStore($PageStorePath)));

# ----------------------------------------
# - Standard Skin Functions
# ----------------------------------------
function dg_SetSkinColor($default, $valid_colors){
global $SkinColor, $ValidSkinColors, $_GET;
	if ( !is_array($ValidSkinColors) ) $ValidSkinColors = array();
	$ValidSkinColors = array_merge($ValidSkinColors, $valid_colors);
	if ( isset($_GET['color']) && in_array($_GET['color'], $ValidSkinColors) )
		$SkinColor = $_GET['color'];
	elseif ( !in_array($SkinColor, $ValidSkinColors) )
		$SkinColor = $default;
	return $SkinColor;
}
function dg_PoweredBy(){
	print ('<a href="http://pmwiki.com/'.($GLOBALS['bi_BlogIt_Enabled']?'Cookbook/BlogIt">BlogIt':'">PmWiki').'</a>');
}
# Determine logo height and width
function dg_SetLogoHeightWidth ($wPad, $hPad=0){
global $PageLogoUrl, $PageLogoUrlHeight, $PageLogoUrlWidth;
	if (!isset($PageLogoUrlWidth) || !isset($PageLogoUrlHeight)){
		$size = @getimagesize($PageLogoUrl);
		if (!isset($PageLogoUrlWidth))  SDV($PageLogoUrlWidth, ($size ?$size[0]+$wPad :0) .'px');
		if (!isset($PageLogoUrlHeight))  SDV($PageLogoUrlHeight, ($size ?$size[1]+$hPad :0) .'px');
	}
}



